import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BreadGUI {
    int totalprice = 0;
    int count = 0;
    StringBuilder cart = new StringBuilder();

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to add " + food + " to your cart?",
                "order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            count += 1;
            cartinfo.setText("number of items in cart:"+count);
            receivedInfo.setText("Added " + food + " to cart.");
            JOptionPane.showMessageDialog(null, "Added " + food + " to cart.");
            cart.append(food);
            cart.append("￥" + price + "\n");
            totalprice += price;
        }
    }

    private JLabel topLabel;
    private JButton applePieButton;
    private JButton baguetteButton;
    private JButton chocolateBreadButton;
    private JButton croissantButton;
    private JButton friedBreadButton;
    private JButton walnutBreadButton;
    private JTextPane receivedInfo;
    private JPanel root;
    private JButton confirmPurchaseButton;
    private JTextPane cartinfo;

    public BreadGUI() {
        applePieButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("apple pie", 378);

            }
        });
        baguetteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("baguette", 205);

            }
        });
        chocolateBreadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("chocolate bread", 259);
            }
        });
        croissantButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("croissant", 226);
            }
        });
        friedBreadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("fried bread", 260);
            }
        });
        walnutBreadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("walnut bread", 194);
            }
        });
        confirmPurchaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to purchase the following items?\n" + cart.toString() + "--total price--\n ￥" + totalprice,
                        "order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    count=0;
                    cartinfo.setText("number of items in cart:"+count);
                    receivedInfo.setText("No order received");
                    JOptionPane.showMessageDialog(null, "Your order has been accepted! Please wait.");
                    totalprice = 0;
                    cart = new StringBuilder();
                }
                if (confirmation == 1) {
                    if (totalprice != 0) {
                        int confirmation2 = JOptionPane.showConfirmDialog(null,
                                "Would you like to reset your order?",
                                "order Confirmation",
                                JOptionPane.YES_NO_OPTION);
                        if (confirmation2 == 0) {
                            count=0;
                            cartinfo.setText("number of items in cart:"+count);
                            receivedInfo.setText("No order received");
                            JOptionPane.showMessageDialog(null, "Order has been reset");
                            totalprice = 0;
                            cart = new StringBuilder();
                        }
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("BreadGUI");
        frame.setContentPane(new BreadGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}


